/**
 * Hasan Diwan licenses this file to you under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * My only request beyond the license is that you let me know how you use this package, and, if you should make any
 * improvements or find/fix bugs, to let me know. Many thanks!
 * @author Hasan Diwan
 */
package us.d8u.ebay;

import java.util.Comparator;
import java.util.Vector;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class EbayComparator implements Comparator<Vector<String>> {

	private static final int DATE_INDEX = 3;

	@Override
	public int compare(Vector<String> o1, Vector<String> o2) {
		int result = 0;
		DateTimeFormatter timeFormatter = ISODateTimeFormat.dateTime();
		DateTime dt1 = timeFormatter.parseDateTime(o1.get(EbayComparator.DATE_INDEX));
		DateTime dt2 = timeFormatter.parseDateTime(o2.get(EbayComparator.DATE_INDEX));
		result = dt1.compareTo(dt2);
		return result;
	}
}