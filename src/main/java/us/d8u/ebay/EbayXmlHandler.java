/**
 * Hasan Diwan licenses this file to you under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * My only request beyond the license is that you let me know how you use this package, and, if you should make any
 * improvements or find/fix bugs, to let me know. Many thanks!
 * @author Hasan Diwan
 */

package us.d8u.ebay;

import java.awt.BorderLayout;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Collections;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class EbayXmlHandler extends DefaultHandler {
	public static final int TITLE_INDEX = 0, LINK_INDEX = 1, PRICE_INDEX = 2, DATE_INDEX = 3;

	private static Logger logger = Logger.getLogger(EbayXmlHandler.class);

	private Vector<String> headers = new Vector<String>();
	private Vector<Vector<String>> data = new Vector<Vector<String>>();

	private String relevant = "";
	public JTextArea statisticsArea = new JTextArea();

	public EbayXmlHandler() {
		this.headers.add("Title");
		this.headers.add("Price");
		this.headers.add("Ends at");
		this.headers.add("Link");
	}

	public Vector<Vector<String>> getData() {
		return this.data;
	}

	public JTextArea getStatisticsArea() {
		return this.statisticsArea;
	}

	public void setStatisticsArea(JTextArea statisticsArea1) {
		this.statisticsArea = statisticsArea1;
	}

	@Override
	public void endDocument() {
		SummaryStatistics stats = new SummaryStatistics();

		for (final Vector auction : this.data) {
			String moneyAsString = (String) auction.get(EbayXmlHandler.PRICE_INDEX);
			EbayXmlHandler.logger.info(String.format("money from table: %s", moneyAsString));
			NumberFormat format = NumberFormat.getCurrencyInstance();
			Number moneyValue = null;
			try {
				moneyValue = format.parse(moneyAsString);
			} catch (ParseException e) {
				EbayXmlHandler.logger.fatal(e.getMessage(), e);
			}
			double value = moneyValue.doubleValue();
			EbayXmlHandler.logger.info("primitive value is " + value);
			stats.addValue(value);
		}
		System.setProperty("Average Price", new Double(stats.getMean()).toString());
		EbayTableModel model = new EbayTableModel();
		Collections.sort(this.data, new EbayComparator());
		model.setDataVector(this.data, this.headers);
		App.tbl.setDefaultRenderer(Object.class, new EbayTableCellRenderer());
		App.tbl.setToolTipText(
				"Green background -- indicates the price is below the mean for the search, red indicates above. The mean is "
						+ NumberFormat.getCurrencyInstance().format(new Double(Math.round(stats.getMean() * 100)) / 100)
						+ ", with a standard deviation of " + stats.getStandardDeviation());
		App.tbl.setModel(model);
		App.tbl.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		App.tbl.setAutoscrolls(true);
		App.tbl.setAutoCreateRowSorter(true);

		JScrollPane scrollPane = new JScrollPane(App.tbl);

		EbayXmlHandler.logger.info("scrollpane created with table!");

		WindowResizeListener mainFrameResizeListener = new WindowResizeListener(scrollPane, App.tbl);

		App.mainFrame.addComponentListener(mainFrameResizeListener);
		EbayXmlHandler.logger.info("custom resize listener added");
		App.tbl.addMouseListener(new TableMouseAdapter(App.tbl));
		EbayXmlHandler.logger.info("mouse listener added!");
		App.tbl.setModel(model);
		EbayXmlHandler.logger.info("model reloaded!");

		JTableHeader header = App.tbl.getTableHeader();
		TableColumnModel columnModel = header.getColumnModel();
		TableColumn linkColumn = columnModel.getColumn(App.tbl.getColumnCount() - 1);
		App.tbl.removeColumn(linkColumn);

		App.mainFrame.add(App.searchField, BorderLayout.NORTH);
		App.searchField.addMouseListener(new SearchFieldPostClickListener());
		EbayXmlHandler.logger.info("search field readded!");
		JPanel scrollPanel = new JPanel();
		scrollPanel.add(scrollPane);
		App.mainFrame.add(scrollPanel, BorderLayout.SOUTH);
		EbayXmlHandler.logger.info("table added");

		App.mainFrame.pack();
		App.mainFrame.setVisible(true);

	}

	@Override
	public void startDocument() {
		this.statisticsArea.setText("Parsing XML");
	}

	@Override
	public void startElement(@SuppressWarnings("unused") String ns, @SuppressWarnings("unused") String localName,
			String qName, @SuppressWarnings("unused") Attributes attrs) {
		EbayXmlHandler.logger.warn("In startelement with " + qName);
		if (qName.equals("itemId")) {
			this.itemData = new Vector<String>();
		}
	}

	@Override
	public void characters(char[] ch, int begin, int end) {
		this.relevant = this.relevant + new String(ch, begin, end);
	}

	Vector<String> itemData = new Vector<String>();

	@Override
	public void endElement(@SuppressWarnings("unused") String ns, @SuppressWarnings("unused") String localName,
			String qName) {
		if (qName.equals("itemId")) {
			EbayXmlHandler.logger
					.info(String.format("ongoing auction: 0-%d elements.", this.itemData.toArray().length));
			this.data.add(this.itemData);
		} else if (qName.equals("convertedCurrentPrice")) {
			Double price = new Double(this.relevant);
			NumberFormat formatter = NumberFormat.getCurrencyInstance();
			String formattedPrice = formatter.format(price.doubleValue());
			this.itemData.add(formattedPrice);
		} else if (qName.equals("title")) {
			this.itemData.add(this.relevant);
		} else if (qName.equals("endTime")) {
			DateTimeFormatter formatter = ISODateTimeFormat.dateTime();
			DateTime date = formatter.parseDateTime(this.relevant);
			this.itemData.add(formatter.print(date));
		} else if (qName.equals("viewItemURL")) {
			this.itemData.add(this.relevant);
		}
		this.relevant = "";
	}

	@Override
	public String toString() {
		return this.data.toString();
	}
}
