/**
 * Hasan Diwan licenses this file to you under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * My only request beyond the license is that you let me know how you use this package, and, if you should make any
 * improvements or find/fix bugs, to let me know. Many thanks!
 * @author Hasan Diwan
 */
package us.d8u.ebay;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SearchFieldActionListener implements ActionListener {
	protected static final Logger logger = Logger.getLogger("us.d8u.ebay.SearchFieldActionListener");
	protected static final String EBAY_URL = "http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=ERM089df1-6a9f-427c-813c-51c02819f1d&RESPONSE-DATA-FORMAT=XML&REST-PAYLOAD&keywords=";

	private DefaultHttpClient client = new DefaultHttpClient();

	private final EbayXmlHandler handler = new EbayXmlHandler();

	public EbayXmlHandler getHandler() {
		return this.handler;
	}

	private static int logQuery(String query) {
		final File f = new File(App.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		byte[] applicationId = PasswordEncryptionService.getEncryptedPassword(f.getName());
		StringBuilder sb = new StringBuilder(2 * applicationId.length);
		for (byte b : applicationId) {
			sb.append("0123456789ABCDEF".charAt((b & 0xF0) >> 4));
			sb.append("0123456789ABCDEF".charAt(b & 0x0F));
		}

		String appId = sb.toString();
		HttpPost post = new HttpPost("http://logging.d8u.us/");
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		Map<String, String> json = new HashMap<String, String>();
		json.put("query", query);
		json.put("app", "eBay");
		String query2 = null;
		try {
			query2 = new ObjectMapper().writeValueAsString(json);
		} catch (JsonProcessingException e) {
			SearchFieldActionListener.logger.fatal(e.getMessage(), e);
		}
		nameValuePairs.add(new BasicNameValuePair("metadata", query2));
		nameValuePairs.add(new BasicNameValuePair("app_id", appId));
		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e2) {
			SearchFieldActionListener.logger.fatal(e2.getMessage(), e2);
		}
		HttpClient client = new DefaultHttpClient();
		HttpResponse resp = null;
		try {
			resp = client.execute(post);
		} catch (ClientProtocolException e1) {
			SearchFieldActionListener.logger.fatal(e1.getMessage(), e1);
		} catch (IOException e1) {
			SearchFieldActionListener.logger.fatal(e1.getMessage(), e1);
		}

		int returnValue = 0;
		if (resp != null) {
			if (resp.getStatusLine() != null) {
				returnValue = resp.getStatusLine().getStatusCode();
			}
		}
		return returnValue;
	}

	public SearchFieldActionListener() {
	}

	@Override
	public void actionPerformed(@SuppressWarnings("unused") ActionEvent arg0) {
		String query = new String();
		try {
			query = URLEncoder.encode(App.searchField.getText(), "utf-8");
		} catch (UnsupportedEncodingException e1) {
			SearchFieldActionListener.logger.fatal(e1.getMessage(), e1);
		}
		HttpGet getMethod = new HttpGet(String.format("%s%s", SearchFieldActionListener.EBAY_URL, query));
		HttpResponse response = null;
		try {
			response = this.client.execute(getMethod);
		} catch (ClientProtocolException e1) {
			SearchFieldActionListener.logger.fatal(e1.getMessage(), e1);
		} catch (IOException e1) {
			SearchFieldActionListener.logger.fatal(e1.getMessage(), e1);
		}
		if (response == null) {
			throw new RuntimeException(SearchFieldActionListener.EBAY_URL + " failed, exiting!");
		}
		final HttpEntity entity = response.getEntity();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = null;
		try {
			parser = factory.newSAXParser();
		} catch (ParserConfigurationException e1) {
			SearchFieldActionListener.logger.fatal(e1.getMessage(), e1);
		} catch (SAXException e1) {
			SearchFieldActionListener.logger.fatal(e1.getMessage(), e1);
		}
		if (parser != null) {
			try {
				parser.parse(entity.getContent(), this.handler);
				SearchFieldActionListener.logQuery(App.searchField.getText());
			} catch (IllegalStateException e) {
				SearchFieldActionListener.logger.fatal(e.getMessage(), e);
			} catch (SAXException e) {
				SearchFieldActionListener.logger.fatal(e.getMessage(), e);
			} catch (IOException e) {
				SearchFieldActionListener.logger.fatal(e.getMessage(), e);
			}
		}
	}
}