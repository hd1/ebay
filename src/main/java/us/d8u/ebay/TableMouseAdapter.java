/**
 * Hasan Diwan licenses this file to you under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * My only request beyond the license is that you let me know how you use this package, and, if you should make any
 * improvements or find/fix bugs, to let me know. Many thanks!
 * @author Hasan Diwan
 */
package us.d8u.ebay;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

public class TableMouseAdapter implements MouseListener {
	private static Logger logger = Logger.getLogger(TableMouseAdapter.class);
	protected static final int LOCATION_INDEX = 3;
	JTable target;

	public TableMouseAdapter(JTable aTable) {
		this.target = aTable;
	}

	@Override
	public void mouseClicked(MouseEvent arg) {
		JPopupMenu ctxMenu = new JPopupMenu();
		if (arg.getClickCount() == 2) {
			int row = this.target.getSelectedRow();
			DefaultTableModel ourColumnModel = (DefaultTableModel) this.target.getModel();
			TableMouseAdapter.logger.warn(ourColumnModel.getValueAt(row, TableMouseAdapter.LOCATION_INDEX));
			Toolkit tk = Toolkit.getDefaultToolkit();
			Clipboard pb = tk.getSystemClipboard();
			String url = (String) ourColumnModel.getValueAt(row, TableMouseAdapter.LOCATION_INDEX);
			StringSelection toClipboard = new StringSelection(url);
			pb.setContents(toClipboard, null);
			JOptionPane.showMessageDialog(null, url + " copied to clipboard");
		}
		if (SwingUtilities.isRightMouseButton(arg) || arg.getModifiers() == InputEvent.CTRL_MASK) {
			ctxMenu.show(this.target, arg.getX(), arg.getY());
		}
		this.target.add(ctxMenu);
	}

	@Override
	public void mousePressed(@SuppressWarnings("unused") MouseEvent e) {
	}

	@Override
	public void mouseReleased(@SuppressWarnings("unused") MouseEvent e) {
	}

	@Override
	public void mouseEntered(@SuppressWarnings("unused") MouseEvent e) {
	}

	@Override
	public void mouseExited(@SuppressWarnings("unused") MouseEvent e) {
	}
}
