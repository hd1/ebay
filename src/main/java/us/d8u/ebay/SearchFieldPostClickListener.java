package us.d8u.ebay;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

public class SearchFieldPostClickListener implements MouseListener {
	private static Logger logger = Logger.getLogger(SearchFieldPostClickListener.class);

	@Override
	public void mouseClicked(MouseEvent e) {
		SearchFieldPostClickListener.logger.toString();
		JTextField sourceObj = (JTextField) e.getSource();
		sourceObj.setText("");
		JOptionPane.showMessageDialog(null,
				"The workaround for restoring functionality is to restart for every search. Apologies for this. If you'd like to help, point your web browser to https://bitbucket.org/hd1/ebay/issues/3/post-search-textfield-ui-error-on.");
	}

	@Override
	public void mousePressed(MouseEvent e) {
		e.getSource();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		e.getSource();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		e.getSource();
	}

	@Override
	public void mouseExited(MouseEvent e) {
		e.getSource();
	}

}
