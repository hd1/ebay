package us.d8u.ebay;

import java.awt.Color;
import java.awt.Component;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.log4j.Logger;

public class EbayTableCellRenderer extends DefaultTableCellRenderer {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger("us.d8u.ebay.EbayTableCellRenderer");

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int col) {
		Component rendererComp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
		EbayTableCellRenderer.logger.info(System.getProperty("Average Price") + " is the average price.");
		String numericalPriceAsString = System.getProperty("Average Price");
		EbayTableCellRenderer.logger.info("The numerical portion of the average price: " + numericalPriceAsString);
		NumberFormat format = NumberFormat.getInstance();
		Double storedMeanPrice = null;
		try {
			storedMeanPrice = format.parse(numericalPriceAsString).doubleValue();
		} catch (ParseException e1) {
			EbayTableCellRenderer.logger.fatal(e1.getMessage(), e1);
		}
		String rawPriceFromTable = (String) table.getModel().getValueAt(row, EbayTableModel.PRICE_INDEX);
		format = NumberFormat.getCurrencyInstance();
		Number priceFromTable = null;
		try {
			priceFromTable = format.parse(rawPriceFromTable);
		} catch (ParseException e) {
			EbayTableCellRenderer.logger.fatal(e.getMessage(), e);
		}
		Double thisPrice = priceFromTable.doubleValue();
		EbayTableCellRenderer.logger.debug("Current price is " + thisPrice);
		if (thisPrice != null && storedMeanPrice != null) {
			boolean goodPrice = thisPrice.doubleValue() < storedMeanPrice.doubleValue();
			if (goodPrice) {
				EbayTableCellRenderer.logger.debug("price is right -- should be green background");
				rendererComp.setBackground(Color.GREEN);
			} else {
				EbayTableCellRenderer.logger.debug("price is too high -- should be red background");
				rendererComp.setBackground(Color.RED);
			}
		}
		return rendererComp;
	}

}
