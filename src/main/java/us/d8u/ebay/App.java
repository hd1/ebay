/**
 * Hasan Diwan licenses this file to you under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * My only request beyond the license is that you let me know how you use this package, and, if you should make any
 * improvements or find/fix bugs, to let me know. Many thanks!
 * @author Hasan Diwan
 */
package us.d8u.ebay;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

public class App {
	static final String EBAY_QUERY_KEY = "us.d8u.ebay.queryKey";
	protected static final Logger logger = Logger.getLogger("us.d8u.ebay.App");
	public static JFrame mainFrame = new JFrame("eBay");
	public static JTextField searchField = new JTextField();
	public static JTable tbl = new JTable();
	public static Vector<Vector<String>> data = new Vector<Vector<String>>();

	public static void main(final String[] args) {
		App.searchField.setEditable(true);
		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new BorderLayout());
		searchPanel.add(App.searchField, BorderLayout.NORTH);
		searchPanel.setSize(App.mainFrame.getSize());

		App.searchField.setSize(searchPanel.getSize());

		SearchFieldActionListener searchFieldActionListener = new SearchFieldActionListener();
		App.searchField.addActionListener(searchFieldActionListener);

		App.mainFrame.setLayout(new BorderLayout());
		App.logger.info("frame layout complete");

		App.mainFrame.add(searchPanel, BorderLayout.NORTH);
		App.logger.info("search field added!");

		App.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		App.mainFrame.pack();
		App.logger.info("frame packed!");

		App.mainFrame.setVisible(true);
	}

}
