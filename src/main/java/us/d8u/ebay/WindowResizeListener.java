/**
 * Hasan Diwan licenses this file to you under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * My only request beyond the license is that you let me know how you use this package, and, if you should make any
 * improvements or find/fix bugs, to let me know. Many thanks!
 * @author Hasan Diwan
 */
package us.d8u.ebay;

import java.awt.Component;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JScrollPane;
import javax.swing.JTable;

public class WindowResizeListener implements ComponentListener {
	private JTable tbl;
	private JScrollPane scrollPane;

	public WindowResizeListener(JScrollPane aScrollPane, JTable aTable) {
		this.tbl = aTable;
		this.scrollPane = aScrollPane;
	}

	@Override
	public void componentResized(ComponentEvent evt) {
		Component c = (Component) evt.getSource();
		this.scrollPane.setSize(c.getSize());
		this.tbl.setSize(this.scrollPane.getSize());
	}

	@Override
	public void componentMoved(@SuppressWarnings("unused") ComponentEvent e) {
	}

	@Override
	public void componentShown(@SuppressWarnings("unused") ComponentEvent e) {
	}

	@Override
	public void componentHidden(@SuppressWarnings("unused") ComponentEvent e) {
	}

}
