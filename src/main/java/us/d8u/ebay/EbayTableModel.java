package us.d8u.ebay;

/**
 * Hasan Diwan licenses this file to you under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * My only request beyond the license is that you let me know how you use this package, and, if you should make any
 * improvements or find/fix bugs, to let me know. Many thanks!
 * @author Hasan Diwan
 */

import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Vector;

import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

public class EbayTableModel extends DefaultTableModel {
	/**
	 *
	 */
	private static final long serialVersionUID = -5094920089179020671L;
	private static final Logger logger = Logger.getLogger(EbayTableModel.class);

	public static final int TITLE_INDEX = 0;
	public static final int PRICE_INDEX = 1;
	public static final int END_TIME_INDEX = 2;
	public static final int LINK_INDEX = 3;

	public EbayTableModel() {
	}

	@Override
	public boolean isCellEditable(@SuppressWarnings("unused") int row, @SuppressWarnings("unused") int column) {
		return false;
	}

	@Override
	public Object getValueAt(int row, int column) {
		JTextField searchField = App.searchField;
		ActionListener[] searchFieldActions = searchField.getActionListeners();
		SearchFieldActionListener firstListener = (SearchFieldActionListener) searchFieldActions[0];
		EbayXmlHandler handler = firstListener.getHandler();
		Vector<String> ourData = handler.getData().get(row);
		switch (column) {
		case TITLE_INDEX:
			return ourData.get(EbayXmlHandler.TITLE_INDEX);
		case PRICE_INDEX:
			String rawAmount = ourData.get(EbayXmlHandler.PRICE_INDEX);
			NumberFormat formatter = NumberFormat.getCurrencyInstance();
			Number returns = null;
			try {
				returns = formatter.parse(rawAmount);
			} catch (ParseException e) {
				EbayTableModel.logger.fatal(e.getMessage(), e);
			}
			return formatter.format(returns);
		case END_TIME_INDEX:
			return ourData.get(EbayXmlHandler.DATE_INDEX);
		case LINK_INDEX:
			return ourData.get(EbayXmlHandler.LINK_INDEX);
		default:
			break;
		}
		return null;
	}
}
